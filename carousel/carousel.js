$.Carousel = function(el) {
	this.$el = $(el);
	this.activeIdx = 0;
	$("div.items img:first-child").addClass("active");
	var that = this;
	$(".slide-left").on("click", function() {
		that.slideLeft();
	});

	$(".slide-right").on("click", function() {
		that.slideRight();
	});

};

$.Carousel.prototype.slide = function(dir) {
	if (this.transitioning) { return; }
	this.transitioning = true;
	var $currentSlide = $(this.$el.find("img").get(this.activeIdx));
	this.activeIdx += dir;
	var imgCount = this.$el.find("img").length;
	if (this.activeIdx < 0) {
		this.activeIdx = imgCount - 1;
	} else if (this.activeIdx === imgCount) {
		this.activeIdx = 0;
	}
	var $newSlide = $(this.$el.find("img").get(this.activeIdx));
	$newSlide.addClass("active");
	if (dir === -1) {
		$newSlide.addClass("left");
		$currentSlide.addClass("right");
	} else if (dir === 1) {
		$newSlide.addClass("right");
		$currentSlide.addClass("left");
	}
	var that = this;
	$currentSlide.one("transitionend", function() {
		$currentSlide.removeClass("active left right");
		that.transitioning = false;
	});

	setTimeout(function() {
		$newSlide.removeClass("left right");
	}, 0);
};

$.Carousel.prototype.slideLeft = function(){
	this.slide(-1);
};

$.Carousel.prototype.slideRight = function(){
	this.slide(1);
};

$.fn.carousel = function () {
	return this.each(function() {
		new $.Carousel(this);
	});
};