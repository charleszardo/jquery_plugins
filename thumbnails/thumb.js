$.Thumbnails = function(el){
	this.$el = $(el);
	this.$activeImg = this.$el.find(".gutter-images img:first-child")
	this.activate(this.$activeImg);
	this.bindEvents();
	this.gutterIdx = 0;
	this.$images = $(".gutter-images").find("img");
	this.fillGutterImages();
}

$.Thumbnails.prototype.activate = function($img){
	$(".active").find("img").remove();
	this.$el.find(".active").append($img.clone());
};

$.Thumbnails.prototype.fillGutterImages = function(){
	$(".gutter-images").find("img").remove();
	for (var i = this.gutterIdx; i < this.gutterIdx + 5; i++) {
		$(".gutter-images").append(this.$images[i]);
	}
};

$.Thumbnails.prototype.bindEvents = function() {
	var that = this;
	$(".gutter-images").on("click", "img", function(event) {
		var $currentTarget = $(event.currentTarget);
		that.$activeImg = $currentTarget;
		that.activate($currentTarget);
	});

	$(".gutter-images").on("mouseenter", "img", function(event){
		var $currentTarget = $(event.currentTarget);
		that.activate($currentTarget);
	});

	$(".gutter-images").on("mouseleave", "img", function(event){
		that.activate(that.$activeImg);
	})

	$(".left").on("click", function(event){
		if (that.gutterIdx === 0){ return;}
		that.gutterIdx -= 1;
		that.fillGutterImages();
	})

	$(".right").on("click", function(event){
		if (that.gutterIdx === that.$images.length - 5){ return;}
		that.gutterIdx += 1;
		that.fillGutterImages();
	})
};

$.fn.thumbnails = function(){
	return this.each(function(){
		new $.Thumbnails(this);
	});
};