$.Tabs = function (el) {
	this.$el = $(el);
	this.$contentTabs = $(this.$el.data("content-tabs"));
	this.$activeTab = this.$el.find("a.active");
	var that = this;
	this.$el.on('click', 'a', function(event) {
		event.preventDefault();
		that.clickTab(event);
	});
};

$.Tabs.prototype.clickTab = function(event){
	var $clickedTab = $(event.currentTarget);
	var $newTabBody = $($clickedTab.attr("href"));
	var $activeTabBody = $(this.$activeTab.attr("href"));
	this.$activeTab.removeClass("active");
	$activeTabBody.removeClass("active").addClass("transitioning");
	var that = this;
	$activeTabBody.one("transitionend", function(){
		$activeTabBody.removeClass("transitioning");
		that.$activeTab = $(event.currentTarget);
		that.$activeTab.addClass("active");
		$newTabBody.addClass("active transitioning");

		setTimeout(function(){
			$newTabBody.removeClass("transitioning");
		}, 0);
	});

};

$.fn.tabs = function () {
	return this.each(function() {
		new $.Tabs(this);
	});
};

